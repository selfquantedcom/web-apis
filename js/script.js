let numKey;
let numHash;

function getTagedPhotos(btnNum){		

   //avoid pop up window on page load 
   if(btnNum !== 4){
      console.log(btnNum);
      console.log(numHash);

      if(btnNum == numHash){
         window.alert("Correct answer. well done!");
      } else{
         window.alert("Incorrect. The correct answer was: " + String(tagValue));
      }
   }

   const list = document.getElementById("list-data");
   list.innerHTML = "";

   const tagHash = {"food": ["asian food", "malaysian food", "chinese food", "japanese food"], "sport": ["football", "american football", "rugby", "handball"], "city": ["beijing", "hong kong", "peking", "ho chi minh"], "country": ["portugal", "spain", "italy", "greece"], "dog": ["german shephard", "colie", "bulldog", "doberman"]}; 
   const keys = ["food", "sport", "city", "country", "dog"];

   numKey = Math.floor(Math.random()*5);
   numHash = Math.floor(Math.random()*4);      

   tagValue = tagHash[keys[numKey]][numHash];   

   // change button text
   document.getElementById("btn0").innerHTML = tagHash[keys[numKey]][0];
   document.getElementById("btn1").innerHTML = tagHash[keys[numKey]][1];
   document.getElementById("btn2").innerHTML = tagHash[keys[numKey]][2];
   document.getElementById("btn3").innerHTML = tagHash[keys[numKey]][3];
   
   fetch("https://api.tumblr.com/v2/tagged?tag=" + tagValue + "&api_key=MdSfLLUGVVe8sc2nbFPSB3LhZ4JmxXSEu5xstQg7M9QqoIQfNp")
   .then(function(response) {
    if (response.status >= 400) {
      throw new Error("Bad response from server");
   }
   return response.json();
})
   .then(function(result) {      

      const items = result.response;

      //for each item, add img to list.
      for (let i = 0; i < items.length; i++){			
         const item = items[i];
         
            //create li and img to append
            try{
               const img = document.createElement("img");   
               const imgSrc = item.photos[0].alt_sizes[5].url;

               img.src = imgSrc;

               const li = document.createElement("li");               
               li.appendChild(img);
               list.appendChild(li);   

            } catch(err){

            }
         }      
         //using the Mansonry library to style picture layout
         let masonry = new Masonry(list, {
            itemSelector: 'li',            
         });
         setTimeout(function() { masonry.layout(); }, 1000);         
      })
   return 
};

//handle user asnwer 
function answer(param){
   const buttonNum = param.id.split("n")[1];
   getTagedPhotos(buttonNum);
}



